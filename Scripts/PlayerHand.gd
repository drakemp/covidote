extends Node2D

var old_cards = []

func update_playerHand(gameState: Dictionary):
    var hand = gameState['playerHand']
    var hand_size = len(hand)
    var offset = 0
    CardGlobals.has_syringe = false
    
    for c in old_cards:
        c.hide()
        self.remove_child(c)
        c.queue_free()
    old_cards.clear()

    for card in hand:
        if card[CardGlobals.SERUM] == "Syringe":
            CardGlobals.has_syringe = true   
        var instance = CardGlobals.card_factory(card)
        #TODO figure out hand positioning
        instance.set_position(Vector2(offset * CardGlobals.card_spacing + 200, CardGlobals.CENTER_Y_HAND))
        self.add_child(instance)
        old_cards.append(instance)
        offset += 1
