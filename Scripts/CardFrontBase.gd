extends Node2D

func _on_CardFrontBase_input_event(viewport, event, shape_idx):
    if event is InputEventMouseButton and event.is_pressed() and event.get_button_index() == BUTTON_LEFT:
        #TODO: bug where you have to click the colored portion???
        var card_name = get_node("../").name
        var play_area = get_node("../../").name
        ServerGlobals.emit_signal("mouse_click", play_area, card_name)
