extends Node2D

const PORT = 1337
var player_next_id = 1
var connected_ids = {}

func get_next_id():
    var ret = player_next_id
    player_next_id += 1
    return "Player"+str(ret)

func _ready():
    self.connect("mouse_click", self, "_mouse_click_handler")

signal mouse_click(play_area, card_name)
var clicked = null

func _mouse_click_handler(play_area, card_name):
    clicked = [play_area, card_name]
    
func get_clicked():
    return clicked
