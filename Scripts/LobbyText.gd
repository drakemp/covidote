extends RichTextLabel

func update_text(playerlist: Dictionary):
    self.clear()
    var id = get_tree().get_network_unique_id()
    for player in playerlist:
        self.add_text(playerlist[player] +" - "+ str(player))
        if player == id: 
            self.add_text(" *")
        self.newline()
    
