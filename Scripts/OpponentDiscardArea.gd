extends Node2D

var old_cards =[]

func update_opponentDiscard(gameState: Dictionary):
    var hands = gameState['opponentDiscard']
    var hands_size = len(hands)
     
    for c in old_cards:
        c.hide()
        self.remove_child(c)
        c.queue_free()
    old_cards.clear()

    var j = 0
    for player in hands:
        #TODO display name for opponent
        var hand = hands[player]
        var hand_size = len(hand)
        var i = 0
        var playername = RichTextLabel.new()
        playername.set_size(Vector2(100,100))
        playername.push_color(Color(0,0,0))
        playername.add_text(player)
        playername.set_position(Vector2(j*CardGlobals.opp_spacing, 200))
        self.add_child(playername)
        for card in hand:
            var instance = null
            if card[CardGlobals.VALUE] == 'X':
                instance = CardGlobals.CARD_BACK.instance()
                instance.set_name(card[CardGlobals.SERUM]+card[CardGlobals.VALUE])
            else:
                instance = CardGlobals.card_factory(card)
            #TODO figure out hand positioning
            instance.set_position(Vector2((i * CardGlobals.card_spacing) + (j * CardGlobals.opp_spacing) + 100, CardGlobals.CENTER_Y_HAND))
            self.add_child(instance)
            old_cards.append(instance) 
            i += 1
        j += 1
