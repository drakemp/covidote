extends Node2D

var local_gamestate = null

func _ready(): 
    if get_tree().is_network_server():
        CardGlobals.create_gamestate()
        update_everyone()
        
func update_everyone():
    for conn in ServerGlobals.connected_ids:
            rpc_id(conn,"update_gamestate",CardGlobals.per_player_gamestate(conn))

sync func update_gamestate(gamestate: Dictionary):
    local_gamestate = gamestate
    $PlayerHand.update_playerHand(gamestate)
    $PlayerDiscardArea.update_playerDiscard(gamestate)
    $OpponentDiscardArea.update_opponentDiscard(gamestate)
    var nextplayerid = gamestate['playerOrder'][gamestate['next']]
    update_status(ServerGlobals.connected_ids[nextplayerid]+"'s turn")
    if nextplayerid == get_tree().get_network_unique_id():
        player_turn()
    else:
        not_turn()
    
    
func player_turn():
    $PlayerMoveMenu.show()
    if CardGlobals.has_syringe:
        $PlayerMoveMenu/ButtonSyringe.show()

func not_turn():
    $PlayerMoveMenu/ButtonSyringe.hide()
    $PlayerMoveMenu.hide()

sync func update_status(message):
    $Status/StatusBox.clear()
    $Status/StatusBox.add_text(message)

##### Helpers
var counter =  0 #used to wait for all responses
func select_card():
    yield(ServerGlobals, "mouse_click")

func get_clicked():
    return ServerGlobals.get_clicked()
    
func find_card(array, card):
    for i in array:
        if i[0] == card[0] and i[1] == card[1]:
            return i
    return null
    
func next_turn():
    CardGlobals.gamestate['next'] = (CardGlobals.gamestate['next'] + 1) % len(ServerGlobals.connected_ids)
    update_everyone()

##### Game logic details
func _on_ButtonDiscard_pressed(): #client button -> do
    rpc("do_discard")
    
sync func do_discard(): #client do
    rpc("update_status", "Select card to discard")
    yield(select_card(),"completed")
    rpc_id(1, "handle_discarded", get_tree().get_network_unique_id(), get_clicked())
    update_status("Waiting on other players...")

sync func handle_discarded(id, clicked): #server handle
    if get_tree().is_network_server():
        if clicked[0] == "PlayerHand":
            counter += 1
            var card0 = clicked[1].split('-')
            var card = find_card(CardGlobals.gamestate["playerHands"][ServerGlobals.connected_ids[id]], card0)
            assert(not card == null)
            CardGlobals.gamestate["playerHands"][ServerGlobals.connected_ids[id]].erase(card)
            CardGlobals.gamestate["playerDiscards"][ServerGlobals.connected_ids[id]].append(card)
        else:   
            rpc_id(id, "update_status", "Select card to discard")
            rpc_id(id, "do_discard")
    if counter == len(ServerGlobals.connected_ids):
        counter = 0
        next_turn()
        
func _on_ButtonSyringe_pressed():
    rpc_id(get_tree().get_network_unique_id(), "do_syringe")

sync func do_syringe():
    rpc("update_status", "Waiting on player...")
    update_status("Select card to syringe")
    yield(select_card(),"completed")
    rpc_id(1, "handle_syringe", get_tree().get_network_unique_id(), get_clicked())

#TODO: syringe out of player hand...
sync func handle_syringe(id, clicked):
    if get_tree().is_network_server():
        if "Discard" in clicked[0]: 
            var playerid = ServerGlobals.connected_ids[id]
            var syringe = find_card(CardGlobals.gamestate["playerHands"][playerid], ["Syringe","0"])
            assert(not syringe == null)
            CardGlobals.gamestate["playerHands"][playerid].erase(syringe)
            var card = null
            var player = null
            var card0 = clicked[1].split('-')
            if clicked[0] == "OpponentDiscardArea":
                #find the selected card
                for i in ServerGlobals.connected_ids.values():
                    card = find_card(CardGlobals.gamestate["playerDiscards"][i], card0)
                    if card != null:
                        player = i
                        break
                assert(player != null)
                assert(card != null)
            elif clicked[0] == "PlayerDiscardArea":
                print(CardGlobals.gamestate["playerDiscards"][playerid])
                card = find_card(CardGlobals.gamestate["playerDiscards"][playerid], card0)
                player = playerid
                assert(card != null)            
            #swap cards
            CardGlobals.gamestate["playerDiscards"][player].erase(card)
            CardGlobals.gamestate["playerDiscards"][player].append(syringe)
            CardGlobals.gamestate["playerHands"][playerid].append(card)
            next_turn()
        else:
            rpc_id(id, "update_status", "Select card to Syringe")
            rpc_id(id, "do_syringe")   
