extends Node2D

func _ready():
    get_tree().connect("network_peer_connected", self, "_player_connected")
    
func _player_connected(id):
    if get_tree().is_network_server():
        print("[server] player connected " + str(id))
        ServerGlobals.connected_ids[id] = ServerGlobals.get_next_id()
        rpc("update_lobby",ServerGlobals.connected_ids)

sync func update_lobby(ids):
    ServerGlobals.connected_ids = ids
    $Lobby/LobbyText.update_text(ids)

func _on_ButtonHost_pressed():
    print("[server] Hosting")
    var host = NetworkedMultiplayerENet.new()
    var res = host.create_server(ServerGlobals.PORT, 7)
    if res != OK: 
        print("[error] Creating server failed")
        return
    get_tree().set_network_peer(host)
    ServerGlobals.connected_ids[host.get_unique_id()] = ServerGlobals.get_next_id()
    $Start.hide()
    $Lobby.show()
    $Lobby/ButtonHostStartGame.show()

func _on_ButtonJoin_pressed():
    print("[client] Joining network")
    var client = NetworkedMultiplayerENet.new()
    client.create_client($Start/TextIpAddr.text,ServerGlobals.PORT)
    get_tree().set_network_peer(client)
    $Start.hide()
    $Lobby.show()

func _on_ButtonHostStartGame_pressed():
    if get_tree().is_network_server():
        rpc('startGame')

sync func startGame():
    var game = preload("res://PlayerView.tscn").instance()
    get_tree().get_root().add_child(game)
    self.hide()
    
func _on_ButtonChangeName_pressed():
    rpc_id(1, "update_name", get_tree().get_network_unique_id(),$Lobby/ButtonChangeName/Name.text)
    
sync func update_name(id, name):
    ServerGlobals.connected_ids[id] = name
    rpc("update_lobby",ServerGlobals.connected_ids)
