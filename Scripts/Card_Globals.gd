extends Node

var _cards = {}
const SERUM = 0
const VALUE = 1
const MAX_PLAYERS = 7
const TABLE_SYRINGE = [0,1,3,3,2,4,6,7]

export var card_spacing = 50
export var CENTER_Y_HAND = 100
export var opp_spacing = 120
const CARD_BACK = preload("res://Assets/CardBack.tscn")
const SYRINGE = preload("res://Assets/Syringe.tscn")
const colors = ['Red', 'Blue', 'Green', 'Purple', 'Pink', 'Gold', 'Orange']
var secret = null

#Player specific variables
var gamestate = null
var has_syringe = false

func _init():
    for color in colors:
        _cards[color] = load("res://Assets/"+color+"/Serum"+color+".tscn")

func card_factory(card):
    if card[SERUM] == "Syringe":
        return SYRINGE.instance()
    var serum = _cards[card[SERUM]]
    var text = RichTextLabel.new()
    text.set_size(Vector2(100,100))
    text.push_color(Color(0,0,0))
    text.add_text(card[VALUE])
    text.set_position(Vector2(-5, -35))
    var inst = serum.instance()
    inst.set_name(card[SERUM]+'-'+card[VALUE])
    inst.add_child(text)
    return inst
    
func create_gamestate():
    gamestate = {"playerHands":{}, "playerDiscards":{}, 
    "playerOrder": ServerGlobals.connected_ids.keys(), "next":0}
    
    for player in ServerGlobals.connected_ids:
        gamestate["playerHands"][ServerGlobals.connected_ids[player]] = []
        gamestate["playerDiscards"][ServerGlobals.connected_ids[player]] = []
    var per_player = 0 #for dealing cards
    var num_players = len(ServerGlobals.connected_ids)
    var skip = 0
    if num_players == MAX_PLAYERS:
        colors.append('Cyan')
    # Handle X cards
    var deck = []
    for color in colors:
        deck.append([color, 'X'])
    deck.shuffle()
    secret = deck.pop_front()
    # Handle syringes
    for i in range(TABLE_SYRINGE[num_players]):
        deck.append(['Syringe', '0'])
    deck.shuffle()
    # Deal
    per_player = len(deck) / num_players
    for player in ServerGlobals.connected_ids.values():
        for i in range(per_player):
            gamestate["playerHands"][player].append(deck.pop_front())
    if len(deck) > 0:
        skip = len(deck)
        print("Deck has left over (skip next): " + str(len(deck)))
        for i in range(len(deck)):
            var player = ServerGlobals.connected_ids.values()[i]
            gamestate["playerHands"][player].append(deck.pop_front())
    # Handle antidotes
    for color in colors:
        for i in range(num_players):
            deck.append([color, str(i+1)])  
    deck.shuffle()
    # deal
    for i in range(skip):
        var player = ServerGlobals.connected_ids.values()[(i+skip)%num_players]
        gamestate["playerHands"][player].append(deck.pop_front())
    
    per_player = len(deck) / num_players
    for player in ServerGlobals.connected_ids.values():
        for i in range(per_player):
            gamestate["playerHands"][player].append(deck.pop_front())

func per_player_gamestate(id:int):
    var playerid = ServerGlobals.connected_ids[id]
    var player_gamestate = {}
    player_gamestate['playerHand'] = gamestate['playerHands'][playerid]
    player_gamestate['playerDiscard'] = gamestate['playerDiscards'][playerid]
    player_gamestate['opponentDiscard'] = gamestate['playerDiscards'].duplicate(true)
    player_gamestate['opponentDiscard'].erase(playerid)
    player_gamestate['playerOrder'] = gamestate['playerOrder']
    player_gamestate['next'] = gamestate['next']
    return player_gamestate
