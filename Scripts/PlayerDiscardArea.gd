extends Node2D

var old_cards = []

func update_playerDiscard(gameState: Dictionary):
    var hand = gameState['playerDiscard']
    var hand_size = len(hand)
    var offset = 0
    
    for c in old_cards:
        c.hide()
        self.remove_child(c)
        c.queue_free()
    old_cards.clear()
    
    for card in hand:
        var instance = null
        if card[CardGlobals.VALUE] == 'X':
            instance = CardGlobals.CARD_BACK.instance()
            instance.set_name(card[CardGlobals.SERUM]+card[CardGlobals.VALUE])
        else:
            instance = CardGlobals.card_factory(card)
        #TODO figure out hand positioning
        instance.set_position(Vector2(offset * CardGlobals.card_spacing + 200, CardGlobals.CENTER_Y_HAND))
        self.add_child(instance) 
        old_cards.append(instance)
        offset += 1
